package AuthorityCrawler.AuthorityCrawler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.nodes.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;


class BeanData
{
	String link;
	String domain;
	int choice;
	int id;
	String moduleName;
	String subCategory;
	
	BeanData(int id,String link,String domain,int choice,String moduleName,String subCategory)
	{
		this.id=id;
		this.link=link;
		this.choice=choice;
		this.moduleName=moduleName;
		this.subCategory=subCategory;
		this.domain=domain;
	}
}

public class Crawler 
{
	MongoClient client;
	DB crawler;
	DBCollection sampleCrawldata;
	DBCollection crawldata;
	DBCollection titleDescription;
	
	public static DBCollection geodata;
	
	DBCursor cursor;
	ExecutorService executor;
	static SimpleDateFormat dft=new SimpleDateFormat("yyyy-MM-dd");

	List<BasicDBObject> coll = new ArrayList<BasicDBObject>(); 
	Crawler()
	{
		client= new MongoClient("178.32.49.5",27017);
		
		crawler=client.getDB("crawler");
		geodata=crawler.getCollection("geodata");	
		sampleCrawldata=crawler.getCollection("sampleCrawldata");
		crawldata=crawler.getCollection("crawldata");
		titleDescription=crawler.getCollection("titleDescription");
	}
	
	void getAuthorityUrls(int choice)
	{
		//1: "Forum"			"Marketplace"	"Webinar"		"Tenders"	"Remote Work"	"Opportunities"			#Buyers
		//2: "Signal"			"Event"  		"Communities"	"Franchise"	"Classifieds"							#News, Network, Tools
		//3: "Blog/Article"																							#News RSS crawler
		switch(choice)
		{
			case 1: coll.add(new BasicDBObject("subCategory", "Forums")); 
					coll.add(new BasicDBObject("subCategory", "Marketplace"));
					coll.add(new BasicDBObject("subCategory", "Webinar"));
					coll.add(new BasicDBObject("subCategory", "Tenders"));
					coll.add(new BasicDBObject("subCategory", "RemoteWork")); 
					coll.add(new BasicDBObject("subCategory", "BusinessOpportunities"));
					break;
					
			case 2:	coll.add(new BasicDBObject("subCategory", "Signals")); 
					coll.add(new BasicDBObject("subCategory", "Events"));
					coll.add(new BasicDBObject("subCategory", "OnlineCommunities"));
					coll.add(new BasicDBObject("subCategory", "Franchise"));
					coll.add(new BasicDBObject("subCategory", "Classifieds")); 
					break;
					
			case 3:	coll.add(new BasicDBObject("subCategory", "BlogArticles")); //please don't add or change this case. this choice is used in metadata.
					break;		
			
		   default:	System.out.println("Invalid Choice");
			   		System.exit(0);	
		}
		
		//System.out.println(new BasicDBObject("status", "false").append("authorityStatus", "false").append("$or", coll).toJson());
		cursor = crawldata.find(new BasicDBObject("status", "false").append("authorityStatus", "false").append("$or", coll));
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		
		int cursorCount=cursor.count();
		//List<BeanData> links=new ArrayList<BeanData>();
		System.out.println("Total Records= "+cursorCount);
		String link;
		int counter=0; ;
		String mainDomain;
		String domain;
		DBObject present;
		if(cursorCount>=100)
		{			
			while(cursor.hasNext())
			{
				present=cursor.next();
				link=present.get("link").toString();
				domain=present.get("domainLink").toString();
				//Extracting the domain name from domainUrl
				try
				{
					mainDomain=domain.substring(domain.indexOf(".")+1, domain.length());
					mainDomain=mainDomain.substring(0, mainDomain.indexOf("."));
				}
				catch(StringIndexOutOfBoundsException e)
				{
					mainDomain=domain.substring(0, domain.indexOf("."));
				}
				
				System.out.println("Record="+(++counter)+" / "+cursorCount+"\t\tMainDomain="+mainDomain+"\tLink="+link);
			
				new MyRunnable(new BeanData(Integer.parseInt(cursor.curr().get("_id").toString()),link,mainDomain,choice,cursor.curr().get("moduleName").toString(),cursor.curr().get("subCategory").toString()), sampleCrawldata, crawldata, titleDescription).run();
					/*links.add(new BeanData(Integer.parseInt(cursor.curr().get("_id").toString()),link,personName,cursor.curr().get("moduleName").toString(),cursor.curr().get("subCategory").toString()));

					if(links.size()==100)
					{
						processAuthorityUrlBatch(links);
						links.clear();
					}*/
			}
			
			/*if(links.size()>0)
			{
				processAuthorityUrlBatch(links);
				links.clear();
			}*/			
		}
	}
	
	/*void processAuthorityUrlBatch(List<BeanData> links)
	{
		executor = Executors.newFixedThreadPool(10);
		
		for (int i = 0; i < links.size(); i++) 
		{			
			Runnable worker = new MyRunnable(links.get(i), sampleCrawldata, crawldata);
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) 
		{		}
		System.out.println("\nFinished all threads");
	}*/
	
	
    public static void main( String[] args )
    {  
    	/* Choices :
    	 * 1. Buyers
    	 * 2. News, Network, Tools
    	 * 3. Blog/Aricle Rss Crawler, Buyers
    	 */
    	int choice=Integer.parseInt(args[0]);
    	System.out.println(choice);
        Crawler object=new Crawler();
        object.getAuthorityUrls(choice);
        object.client.close();
        object=null;
        System.gc();
        try 
        {
        	System.out.println("Sleeping for 2 hrs.....");
			Thread.sleep(3600000*2);  //sleep for two hrs
			main(args);
        } 
        catch (InterruptedException e) 
        {
			e.printStackTrace();
		}
        /*List<BeanData> links=new ArrayList<BeanData>();
        links.add(new BeanData(1, "http://www.reed.co.uk/jobs/grader-gerber-vetigraph/30946883#/jobs/freelance-jobs-in-london", "", "Buyers", "Remote Work"));
        object.processAuthorityUrlBatch(links);*/
    }
}

class MyRunnable //implements Runnable 
{	
	Document doc;
	MetaData metaDataObject;
	DBCollection sampleCrawldata, crawldata, titleDescription;
	BasicDBObject result;
	BasicDBObject duplicateChk= new BasicDBObject();
	BeanData object;
	boolean duplicateStatus;
	
	MyRunnable(BeanData object, DBCollection sampleCrawldata, DBCollection crawldata, DBCollection titleDescription) 
	{
		this.object = object;
		this.sampleCrawldata = sampleCrawldata;
		this.crawldata = crawldata;
		this.titleDescription = titleDescription;
		
	}

	public void run() 
	{
		
		MetaData metaData=new MetaData();
		try 
		{			
			doc=metaData.jsoupGetResponse(object.link);
			
			if(doc==null)
			{			
				System.out.println("Redirect Domain= "+object.link);
				crawldata.update(new BasicDBObject("link", object.link),  new BasicDBObject("$set",new BasicDBObject("authorityStatus","redirect")));
				sampleCrawldata.update(new BasicDBObject("link", object.link),  new BasicDBObject("$set",new BasicDBObject("authorityStatus","redirect")));
			}else if(MyRunnable.metaRobotsChecks(doc) == false)
			{
				System.out.println("NoIndex link= "+object.link);
				crawldata.update(new BasicDBObject("link", object.link),  new BasicDBObject("$set",new BasicDBObject("authorityStatus","noIndex")));
				sampleCrawldata.update(new BasicDBObject("link", object.link),  new BasicDBObject("$set",new BasicDBObject("authorityStatus","noIndex")));
			}
			else
			{
				result = metaData.processDocument(doc,object.choice,object.domain);
				
				if(!result.get("description").toString().trim().equals(""))
				{
					try
					{
						duplicateChk.clear();
										
						duplicateChk = new BasicDBObject("titleDescription", (result.get("authorityTitle").toString()+result.get("description").toString()).replaceAll("\\s*", ""));
						
						if (!titleDescription.find(duplicateChk).hasNext()) 
						{
							titleDescription.insert(duplicateChk);
							crawldata.update(new BasicDBObject("link", object.link),  new BasicDBObject("$set",result));
							sampleCrawldata.update(new BasicDBObject("link", object.link),  new BasicDBObject("$set",result));
							System.out.println("************\nInserted Link="+object.link+"\n"+result.toJson()+"\nLocation="+result.getString("location")+"\tgeoip="+result.get("geoip").toString()+"\n************\n");
						}
						else
						{
							System.out.println("Already present in titledescription");
							result.put("authorityStatus", "duplicate");
							result.put("status", "false");
							sampleCrawldata.update(new BasicDBObject("link", object.link), new BasicDBObject("$set",result));
							crawldata.update(new BasicDBObject("link", object.link), new BasicDBObject("$set",result));
							System.out.println("************\nDuplicate Link="+object.link+"\n"+result.toJson()+"\nLocation="+result.getString("location")+"\tgeoip="+result.get("geoip").toString()+"\n************\n");
							System.out.println("\n**********************************************************\n");
						}	
					}
					catch(Exception e)
					{
						System.out.println("Exception occurred in checking titledescription duplicate or inserting in crawldata");
						e.printStackTrace();
					}
				}
				else
				{
					System.out.println("MainContent="+result.get("mainContent").toString());
					sampleCrawldata.update(new BasicDBObject("link", object.link), new BasicDBObject("$set",new BasicDBObject("authorityStatus", "blankDescription").append("status", "false")));
					crawldata.update(new BasicDBObject("link", object.link), new BasicDBObject("$set",new BasicDBObject("authorityStatus", "blankDescription").append("status", "false")));
					
				}	
			}		
		} 
		catch (Exception e) 
		{
			sampleCrawldata.update(new BasicDBObject("link", object.link), new BasicDBObject("$set",new BasicDBObject("authorityStatus", "exception").append("exception", e.getClass().getName()+"-"+e.getMessage())));
			crawldata.update(new BasicDBObject("link", object.link), new BasicDBObject("$set",new BasicDBObject("authorityStatus", "exception").append("exception", e.getClass().getName()+"-"+e.getMessage())));
			
			System.out.println("error in run(): "+e.getClass().getName()+"\t "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static boolean metaRobotsChecks(Document doc)
	{
		// Add all the links o=in db related to this domain
		try 
		{
			String robots = doc.select("META[NAME=robots]").get(0).attr("content");
			if (robots.contains("noindex")) 
			{
				return false;
			}
			else 
			{
				return true;
			}
		} 
		catch (Exception e) 
		{
			//System.out.println(e.getClass().getName() + e.getMessage());
			//e.printStackTrace();
		}
		return true;
	}

}
