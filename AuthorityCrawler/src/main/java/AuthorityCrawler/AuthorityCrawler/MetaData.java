package AuthorityCrawler.AuthorityCrawler;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import NER.CheckForTranslation;
import NER.GeoLocation;
import NER.StanFordNER;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import de.l3s.boilerpipe.extractors.KeepEverythingExtractor;


public class MetaData 
{	
	HashMap hm;
	StanFordNER stanford = new StanFordNER();
	String location,leadDate,personName, companyName;
			
	
	public Document jsoupGetResponse(String url) throws IOException
	{
		
		Document doc=null;
		Response response = Jsoup.connect(url)
				.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2")
				.referrer("http://www.google.com") 
				.header("Accept-Language", "en")
				.timeout(180000)
				.followRedirects(true)
				.execute();
		
		/*try
		{
			temp = response.url().toString().split("/")[3];
		}
		catch(Exception e)
		{			
			return doc; 
		}*/
		
		/*try
		{
			mainDomain=url.substring(url.indexOf(".")+1, url.length());
			mainDomain=mainDomain.substring(0, mainDomain.indexOf("."));
		}
		catch(Exception e)
		{
			mainDomain = url.substring(0,url.indexOf("."));
		}
		
		try
		{
			newDomain=response.url().toString().substring(response.url().toString().indexOf(".")+1, response.url().toString().length());
			newDomain=newDomain.substring(0, newDomain.indexOf("."));
		}
		catch(Exception e)
		{
			
			newDomain = response.url().toString().substring(0,(response.url().toString().indexOf(".")));
			//e.printStackTrace();		
		}
		//System.out.println("MainDomain : "+mainDomain);
		//System.out.println("NewDomIN : "+newDomain);
		
		if (mainDomain.equalsIgnoreCase(newDomain) )
		{		
			doc = response.parse();
		}*/	
		
		//System.out.println("URL="+url+"\nResponse="+response.url().toString());
		if(response.url().toString().replaceAll("https?","").equalsIgnoreCase(url.replaceAll("https?","")))
		{
			doc = response.parse();
			return doc;
		}			
		else
			return doc;
	}
	
	//to get map of values;
	public BasicDBObject processDocument(Document doc,int choice,String mainDomain)
	{
		Map<String, String> bean = new LinkedHashMap<String,String>();
		
		String authorityTitle="",description="",keywords="",mainContent="",keepEverything="";
		
		//Site Title
		authorityTitle=doc.title();
		

		//Keywords and Description from Metadata Tag
		for(Element meta : doc.select("meta"))
		{
			if(meta.attr("name").toLowerCase().equals("keyword") || meta.attr("name").toLowerCase().equals(("keywords")))
			{
				keywords=(keywords.trim().equals("")? meta.attr("content"): (keywords+", "+meta.attr("content")) );
			}
			if(meta.attr("name").toLowerCase().equals("description") || meta.attr("name").toLowerCase().equals("descriptions"))
			{
				description+=" "+meta.attr("content");
			}
		}
		
		System.out.println("MetaDescription="+description);

		//Main content from boilerpipe.
		try 
		{
			mainContent = ArticleExtractor.INSTANCE.getText(doc.toString());
		}
		catch (BoilerpipeProcessingException e) 
		{
			
		}
		
		try 
		{
			keepEverything = KeepEverythingExtractor.INSTANCE.getText(doc.toString());
		} 
		catch (BoilerpipeProcessingException e) 
		{
			System.out.println(e.getClass().getName()+"\t"+e.getMessage());
		}
		
		if(mainContent.trim().equals(""))
		{
			mainContent=keepEverything;
		}
		
		if(mainContent.trim().equals(""))
		{
			mainContent=doc.text();
		}

		//Trim Main & keepEverything Content
		description=description.replaceAll("[\\s]+", " ").trim();
		mainContent=mainContent.replaceAll("[\\s]+", " ").trim();	
		keywords=keywords.replaceAll("[\\s]+", " ").trim();
		
		String phones = getPhone(keepEverything);
		String mails = getEmailId(keepEverything,mainDomain);

		description=StringUtils.stripAccents(description);
		description=description.replaceAll("[^0-9A-Za-z ]", " ").replaceAll(" +"," ").trim();
		
		//If description is empty
		if(description.trim().equals(""))	
		{
			description=mainContent;
			
		}

		description=StringUtils.stripAccents(description);
		description=description.replaceAll("[^0-9A-Za-z ]", " ").replaceAll(" +"," ").trim();
		

		
		location=""; leadDate=""; personName=""; companyName="";
		if(!mainContent.trim().equals(""))
		{	
			hm = stanford.getDataForAllClasses(WordUtils.capitalize(mainContent.toLowerCase()));
		
			leadDate = hm.get("date").toString();
			location = hm.get("location").toString();
			personName = hm.get("person").toString();		
			companyName = hm.get("organization").toString();
			//System.out.println("Ner Op=> date="+leadDate+"**location="+location+"**personName="+personName+"**companyName="+companyName+"**");
			if (leadDate.length() > 2)
				leadDate = leadDate.substring(1, leadDate.length() - 1);
			else
				leadDate = "";

			if (location.length() > 2)
				location = location.substring(1, location.length() - 1);
			else
				location = "";

			if (personName.length() > 2)
				personName = personName.substring(1, personName.length() - 1);
			else
				personName = "";

			if (companyName.length() > 2)
				companyName = companyName.substring(1, companyName.length() - 1);
			else
				companyName = "";
		}
		
		
		String[] geoData=GeoLocation.updateLocation(location);
		//System.out.println("Location="+location+"\ngeoip="+geoData[1]);
		// For geoip field in DB. Structure for ElasticSearch Query
		DBObject dbObject = (DBObject) JSON.parse(geoData[1]);	
		
		//Set fields to Bean
		
		//Authority Crawler
		bean.put("description",description);
		bean.put("keyword",keywords);
		bean.put("authorityTitle",authorityTitle);
		
		//BoilerPipe Crawler
		bean.put("mainContent",mainContent);	
		bean.put("companyEmail",mails.trim());
		bean.put("companyNumber",phones.trim());	
		
		
		//NER Crawler
		bean.put("leadDate", leadDate);
		bean.put("location", location);
		bean.put("geoLocation", geoData[0]);
		
		if(choice!=3) //Don't add person name for Blog/Article subcategory.
		{
			bean.put("personName", personName);
		}
		
		bean.put("companyName", companyName);
		bean.put("relatedWebsites", "");
		
		//Set Status
		bean.put("translationStatus", CheckForTranslation.detectLanguage(mainContent));
		bean.put("authorityStatus", "true");
		bean.put("boilerpipeStatus", "true");
		bean.put("nerStatus", "true");
		bean.put("status", "true");
		bean.put("updatedBy", "AuthorityCrawler");
		
		BasicDBObject insertObject = new BasicDBObject();
		
		// Add all bean fields.
		insertObject.putAll(bean);
		insertObject.put("geoip",dbObject);
		
		return insertObject;
	}

	public String getEmailId(String textProcess,String mainDomain)
	{
		textProcess=textProcess.replace("[at]", "@").replace("(at)", "@").replace(" @ ", "@");
		String emailId="";
		Set<String> email = new HashSet<String>();
		Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+ *?@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(textProcess);
		while (m.find()) 
		{	if(!m.group().contains(mainDomain))		
			{
				email.add(m.group().toLowerCase().trim());				
			}
		}

		if(!email.isEmpty()) //if emailId is present
		{
			for(String e1:email)
			{
				if(!e1.equals("")) // checking emailID if it null
				{
					emailId+=e1+",";
					//System.out.println("inside"+emailId);
				}
			}
			if(!emailId.isEmpty())
			{
				emailId = emailId.substring(0,emailId.length()-1); //Remove the last comma(,)
			}
		}
		//System.out.println("!!!!!!!!!!!!!!!!!"+emailId);
		return emailId;
	}
	
	public String getPhone(String textProcess)
	{
		String phone="";
		Set<String> phn = new HashSet<String>();
		// Regex for phone
		Matcher m1 = Pattern.compile("\\D*:?\\+?[ 0-9. ()-]{10,25}").matcher(textProcess);
		String num;
		while (m1.find()) 
		{
			num=m1.group().replaceAll("[^0-9]","").trim();// Remove the starting and ending spaces only take phone number others are remove like(,.;:'"?/()*+-_%$#!=))
			if(num.length() <= 15 && num.length() >= 10) //constraints for phone number
			{
				phn.add(num);
			}
		}
		if(!phn.isEmpty()) //if the phone number is present
		{
			for(String p:phn)
			{
				phone+=p+","; 
			}
			phone= phone.substring(0, phone.length()-1);  //Remove the last comma(,)

		}	
		return phone;
	}
	
	public static void main(String[] args) throws IOException 
	{
		String authLink = "http://www.bizjournals.com/buffalo/news/2016/11/30/one-of-buffalos-earliest-web-development-shops.html";
		MetaData object=new MetaData();
		Document doc=object.jsoupGetResponse(authLink);
		System.out.println(doc);
		BasicDBObject obj=object.processDocument(doc,3,"bizjournals");
		System.out.println(obj.toJson());
	}

}
